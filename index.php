<?php require_once './code.php' ?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S04: Access Modifiers and Encapsulation</title>
	</head>
	<body>
		<h1>Access Modifiers</h1>

		<h2>Building Variable</h2>
		<p><?php //echo $building->name; ?></p>

		<h2>Condominium Variables</h2>
		<p><?php //echo $condominium->name; ?></p>

		<h1>Ecapsulation</h1>
		<p>The name of the condominium is <?php echo $condominium->getName(); ?></p>
		<?php $condominium->setName('Enzo Tower') ?>
		<p>The name of the condominium has been changed to <?php echo $condominium->getName(); ?></p>

		<!-- Mini Activity -->
		<p>The number of floors in the condominium is <?php echo $condominium->getFloors(); ?>. </p>
		<?php $condominium->setFloors(10); ?>
		<p>The new number of floors in the condominium has been changed to <?php echo $condominium->getFloors(); ?>. </p>

	</body>
</html>